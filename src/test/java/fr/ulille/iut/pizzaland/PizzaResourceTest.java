package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import fr.ulille.iut.pizzaland.resources.PizzaResource;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

public class PizzaResourceTest extends JerseyTest {
	private PizzaDao dao;
	private IngredientDao ingDao;
	
	@Override
	protected Application configure() {
		BDDFactory.setJdbiForTests();
		ResourceConfig rc = new ResourceConfig(PizzaResource.class);
		// Dé-commenter pour avoir la trace des requêtes et réponses
		rc.register(new LoggingFeature(Logger.getLogger(LoggingFeature.DEFAULT_LOGGER_NAME),
				Level.INFO,
				LoggingFeature.Verbosity.PAYLOAD_ANY, 10000));
		// Dé-commenter pour avoir une trace des erreurs internes serveur (les tests sur
		// le code de retour vont échouer)
		rc.register(DebugMapper.class);
		return new ApiV1();
	}

	// Les méthodes setEnvUp() et tearEnvDown() serviront à terme à initialiser la
	// base de données
	// et les DAO

	// https://stackoverflow.com/questions/25906976/jerseytest-and-junit-throws-nullpointerexception
	@Before
	public void setEnvUp() {
		ingDao = BDDFactory.buildDao(IngredientDao.class);
		ingDao.createTable();
		dao = BDDFactory.buildDao(PizzaDao.class);
		dao.createTableAndIngredientAssociation();
	}

	@After
	public void tearEnvDown() throws Exception {
		dao.dropTable();
		ingDao.dropTable();
	}

	@Test
	public void testGetEmptyList() {
		Response response = target("/pizzas").request().get();

		// On vérifie le code de la réponse (200 = OK)
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		// On vérifie la valeur retournée (liste vide)
		// L'entité (readEntity() correspond au corps de la réponse HTTP.
		// La classe jakarta.ws.rs.core.GenericType<T> permet de définir le type
		// de la réponse lue quand on a un type paramétré (typiquement une liste).
		List<PizzaDto> pizzas;
		pizzas = response.readEntity(new GenericType<List<PizzaDto>>() {
		});

		assertEquals(0, pizzas.size());

	}

	@Test
	public void testGetExistingPizza() {

		Pizza pizza = new Pizza();
		pizza.setName("Chorizo");
		
		List<Ingredient> list = new ArrayList<>();
		list.add(new Ingredient("Fromage"));
		pizza.setIngredients(list);
		//System.out.println("----------");
		dao.ajoutPizza(pizza);
		
		Response response = target("/pizzas").path(pizza.getId().toString())
				.request(MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		Pizza result = Pizza.fromDto(response.readEntity(PizzaDto.class));
		assertEquals(pizza, result);
		assertEquals(list, result.getIngredients());
		//System.out.println("----------");
	}

	@Test
	public void testGetNotExistingPizza() {
		Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().get();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void testCreatePizza() {
		PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
		pizzaCreateDto.setName("Chorizo");
		
		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

		PizzaDto returnedEntity = response.readEntity(PizzaDto.class);

		assertEquals(target("/pizzas/" + returnedEntity.getId()).getUri(), response.getLocation());
	}

	@Test
	public void testCreatePizzaWithIngredients() {
		PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();
		pizzaCreateDto.setName("Chorizo");
		List<Ingredient> list = new ArrayList<>();
		list.add(new Ingredient("Fromage"));
		list.add(new Ingredient("œufs"));
		pizzaCreateDto.setIngredients(list);
		
		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

		PizzaDto returnedEntity = response.readEntity(PizzaDto.class);

		assertEquals(target("/pizzas/" + returnedEntity.getId()).getUri(), response.getLocation());
		//System.out.println("post post"+returnedEntity.getIngredients());
		assertEquals(2, returnedEntity.getIngredients().size());
	}
	
	@Test
	public void testCreateSamePizza() {
		Pizza pizza = new Pizza();
		pizza.setName("Chorizo");
		
		dao.ajoutPizza(pizza);

		PizzaCreateDto pizzaCreateDto = Pizza.toCreateDto(pizza);
		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
	}

	@Test
	public void testCreatePizzaWithoutName() {
		PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();

		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	}

	@Test
	public void testDeleteExistingPizza() {
		Pizza pizza = new Pizza();
		pizza.setName("Chorizo");
		dao.ajoutPizza(pizza);

		Response response = target("/pizzas/").path(pizza.getId().toString()).request().delete();

		assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

		Pizza result = dao.selectById(pizza.getId());
		assertEquals(result, null);
	}

	@Test
	public void testDeleteNotExistingPizza() {
		Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().delete();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void testGetPizzaName() {
		Pizza pizza = new Pizza();
		pizza.setName("Chorizo");
		dao.ajoutPizza(pizza);
		
		Response response = target("pizzas").path(pizza.getId().toString()).path("name").request().get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		assertEquals("Chorizo", response.readEntity(String.class));
	}

	@Test
	public void testGetNotExistingPizzaName() {
		Response response = target("pizzas").path(UUID.randomUUID().toString()).path("name").request().get();

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void testCreateWithForm() {
		Form form = new Form();
		form.param("name", "chorizo");
		
		Entity<Form> formEntity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE);
		Response response = target("pizzas").request().post(formEntity);

		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
		String location = response.getHeaderString("Location");
		String id = location.substring(location.lastIndexOf('/') + 1);
		Pizza result = dao.selectById(UUID.fromString(id));

		assertNotNull(result);
	}

}
