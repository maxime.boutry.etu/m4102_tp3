package fr.ulille.iut.pizzaland.resources;

import java.net.URI;
import java.util.UUID;


import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;

@Path("/Pizza")
public class PizzaResource {
	
	private PizzaDao pizza;
	
	@Context
	public UriInfo uriInfo;
	
	@GET
	@Path("{id}")
	public Pizza getPizza(UUID id) {
		return pizza.selectById(id);
	}
	
	@POST
	public Response createPizza(PizzaCreateDto pcDto) {
		Pizza meh = pizza.selectByName(pcDto.getName());
		if(meh != null) throw new WebApplicationException(Response.Status.CONFLICT);
	
		try {
			Pizza pizza = Pizza.fromIngredientCreateDto(pcDto);
			this.pizza.ajoutPizza(pizza);
			
			PizzaDto pDto = Pizza.toDto(pizza);
			
			URI uri = uriInfo.getAbsolutePathBuilder().path(pizza.getId().toString()).build();
			
			return Response.created(uri).entity(pDto).build();
		} catch(Exception e) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		}
		
	}
	
}
