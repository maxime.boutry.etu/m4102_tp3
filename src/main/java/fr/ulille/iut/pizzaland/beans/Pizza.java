package fr.ulille.iut.pizzaland.beans;

import java.util.List;
import java.util.UUID;

import fr.ulille.iut.pizzaland.dto.IngredientCreateDto;
import fr.ulille.iut.pizzaland.dto.IngredientDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {
	
	private UUID id = UUID.randomUUID();
	private String name;
	private List<Ingredient> ingredients;
	
	public Pizza() {}
	public Pizza(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}
	
	public Pizza(List<Ingredient> ingredients, UUID id) {
		this.ingredients = ingredients;
		this.id = id;
	}
	
	public Pizza(List<Ingredient> ingredients, String name) {
		this.ingredients = ingredients;
		this.name = name;
	}
	
	public Pizza(List<Ingredient> ingredients, String name, UUID id) {
		this.id = id;
		this.ingredients = ingredients;
		this.name = name;
	}
	
	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}
	public List<Ingredient> getIngredients() {
		return ingredients;
	}
	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public static PizzaDto toDto(Pizza i) {
	    	PizzaDto pdto = new PizzaDto();
	    	pdto.setId(i.getId());
	    	pdto.setIngredients(i.getIngredients());
	    	return pdto;
	    }
	    
	    public static Pizza fromDto(PizzaDto dto) {
	    	Pizza pizza = new Pizza();
	    	pizza.setId(dto.getId());
	    	pizza.setIngredients(dto.getIngredients());
	    	return pizza;
	    }

	    public static PizzaCreateDto toCreateDto(Pizza ingredient) {
	    	PizzaCreateDto pcdto = new PizzaCreateDto();
	    	pcdto.setIngredients(ingredient.getIngredients());
	    	return pcdto;
	    }
	    
	    public static Pizza fromIngredientCreateDto(PizzaCreateDto dto) {
	    	Pizza pizza = new Pizza();
	    	pizza.setIngredients(dto.getIngredients());
	    	return pizza;
	    }
	
}

