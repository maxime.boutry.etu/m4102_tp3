package fr.ulille.iut.pizzaland.dao;

import java.util.UUID;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import fr.ulille.iut.pizzaland.beans.Pizza;

public interface PizzaDao {
	
  @SqlUpdate("CREATE TABLE IF NOT EXISTS Pizzas(id VARCHAR(128) PRIMARY KEY, name VARCHAR UNIQUE NOT NULL)")
  void createPizzaTable();

  @SqlUpdate("CREATE TABLE IF NOT EXISTS PizzaIngredientsAssociation(idPizza VARCHAR(128), idIngredient VARCHAR(128), FOREIGN KEY(idPizza) REFERENCES Pizza(id), FOREIGN KEY(idIngredient) REFERENCES ingredients(id), PRIMARY KEY(idPizza,idIngredient));")
  void createAssociationTable();

  @SqlUpdate("DROP TABLE IF EXISTS Pizzas")
  void dropTable();
  
  @SqlUpdate("DROP TABLE IF EXISTS PizzaIngredientsAssociation")
  void dropAssociation();
  
  @Transaction
  default void createTableAndIngredientAssociation() {
    createAssociationTable();
    createPizzaTable();
  }
 
  //@SqlQuery("Insert into Pizzas VALUES")
  //void fraise();
  
  @SqlQuery("SELECT * FROM Pizzas Where id=:id")
  @RegisterBeanMapper(Pizza.class)
  Pizza selectById(@Bind("id") UUID id);
  
  @SqlQuery("SELECT * FROM Pizzas Where name=:name")
  @RegisterBeanMapper(Pizza.class)
  Pizza selectByName(@Bind("name") String name);
  
  @SqlUpdate("Insert into Pizzas values (:id, :name)")
  void ajoutPizza(@BindBean Pizza pizza );
  
  
}