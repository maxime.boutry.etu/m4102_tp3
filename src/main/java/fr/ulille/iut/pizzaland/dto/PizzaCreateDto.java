package fr.ulille.iut.pizzaland.dto;

import java.util.List;

import fr.ulille.iut.pizzaland.beans.Ingredient;

public class PizzaCreateDto {

	private List<Ingredient> ingredients;
	private String name;
	
	public PizzaCreateDto() {
		
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}
	
	public String getName() {
		return this.name;
	}
	
	public List<Ingredient> getIngredients(){
		return this.ingredients;
	}
	
}
