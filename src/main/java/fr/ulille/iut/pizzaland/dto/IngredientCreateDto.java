package fr.ulille.iut.pizzaland.dto;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.core.Response;

public class IngredientCreateDto {
	private String name;
		
	public IngredientCreateDto() {}
		
	public void setName(String name) {
		this.name = name;
	}
 		
	public String getName() {
		return name;
	}
	
	
}
